RestDungeon

Dans cette génération de donjon il y a des clés pour accéder à des salles verrouillés et des trésors. 

Pour générer les salles du donjon, a été utilisé un arbre de Binary Space Partitioning. Cet arbre à permis de diviser les salles de manière récursive et de leur donner des dimensions aléatoires.

À chaque salle correspond un type. Ce type indique si la salle contient une clé, est verrouillée, contient un trésor ou un monstre. Pour générer les types de salles à été utilisée une grammaire:
donjon -> obstacle + clé + trésor;
obstacle -> clé + obstacle + salle verrouillée + obstacle;
obstacle -> monstre + obstacle;
obstacle -> salle
Avoir utilisé une grammaire à permis d’attribuer une clé à chaque salle verrouillé ou trésor.


