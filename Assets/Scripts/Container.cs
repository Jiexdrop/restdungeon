﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;
using Random = UnityEngine.Random;
using UnityEngine.Tilemaps;

public class Container
{
    public int x;
    public int y;
    public int width;
    public int height;

    public Vector2Int center;

    float RATIO = 0.45f;

    public Container(int x, int y, int width, int height)
    {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;

        center = new Vector2Int(x + (width / 2), y + (height / 2));
    }

    public void drawContainer(Tilemap tilemap, TileBase[] tiles)
    {
        for (int i = x; i < x + width; i++) // Top Wall 
        {
            tilemap.SetTile(new Vector3Int(i, y + height, 0), tiles[0]);
        }

        for (int i = x; i < x + width; i++) // Bottom Wall
        {
            tilemap.SetTile(new Vector3Int(i, y, 0), tiles[0]);
        }

        for (int i = y; i < y + height; i++) // Left wall
        {
            tilemap.SetTile(new Vector3Int(x, i, 0), tiles[0]);
        }

        for (int i = y; i <= y + height; i++) // Right wall
        {
            tilemap.SetTile(new Vector3Int(x + width, i, 0), tiles[0]);
        }
    }

    public Tree splitContainer(Container c, int iter)
    {
        Tree root = new Tree(c);

        if(iter != 0)
        {
            Tuple<Container, Container> split = randomSplit(c);
            root.leftChild = splitContainer(split.Item1, iter - 1);
            root.rightChild = splitContainer(split.Item2, iter - 1);
        }

        return root;
    }

    Tuple<Container, Container> randomSplit(Container c)
    {
        if (Random.Range(0, 2) < 1)
        {
            // Vertical
            Container c1 = new Container(c.x, c.y, Random.Range(1, c.width), c.height);
            Container c2 = new Container(c.x + c1.width, c.y, c.width - c1.width, c.height);
  
            //Debug.Log("V:" + c1.ToString() + "///" + c2.ToString());

            float r1 = (float)(c1.width) / c1.height;
            float r2 = (float)(c2.width) / c2.height;

            if (r1 < RATIO || r2 < RATIO)
            {
                return randomSplit(c);
            }

            return new Tuple<Container, Container>(c1, c2);
        }
        else
        {
            // Horizontal
            Container c1 = new Container(c.x, c.y, c.width, Random.Range(1, c.height));
            Container c2 = new Container(c.x, c.y + c1.height, c.width, c.height - c1.height);

            //Debug.Log("H:" + c1.ToString() + "///" + c2.ToString());

            float r1 = (float)(c1.height) / c1.width;
            float r2 = (float)(c2.height) / c2.width;

            if (r1 < RATIO || r2 < RATIO)
            {
                return randomSplit(c);
            }

            return new Tuple<Container, Container>(c1, c2);
        }
    }

    public void createPath(Container container, Tilemap tilemap, TileBase[] tiles)
    {
        //Debug.Log(center + "-" + container.center);
        for (int i = center.x; i <= container.center.x; i++)
        {
            for (int j = center.y; j <= container.center.y; j++)
            {
                Vector3Int pos = new Vector3Int(i + 1, j, 0);
                if (!tilemap.HasTile(pos))
                {
                    tilemap.SetTile(pos, tiles[0]);
                }
                pos = new Vector3Int(i - 1, j, 0);
                if (!tilemap.HasTile(pos))
                {
                    tilemap.SetTile(pos, tiles[0]);
                }
                pos = new Vector3Int(i, j + 1, 0);
                if (!tilemap.HasTile(pos))
                {
                    tilemap.SetTile(pos, tiles[0]);
                }
                pos = new Vector3Int(i, j - 1, 0);
                if (!tilemap.HasTile(pos))
                {
                    tilemap.SetTile(pos, tiles[0]);
                }
                
            }
        }

        for (int i = center.x; i <= container.center.x; i++)
        {
            for (int j = center.y; j <= container.center.y; j++)
            {
                Vector3Int pos = new Vector3Int(i, j, 0);
                // Only replace if tile equals walls
                if (tilemap.GetTile(pos).Equals(tiles[0]))
                {
                    tilemap.SetTile(pos, tiles[1]);
                }

            }
        }
       
    }

    public override string ToString()
    {
        return "(x:" + x + ", y:" + y + ", w:" + width + ", h:" + height + ")";
    }
}