﻿using UnityEngine;
using System.Collections;
using UnityEngine.Tilemaps;
using System.Collections.Generic;
using System;

public class Tree
{
    public Container leaf;
    public Tree leftChild;
    public Tree rightChild;

    public Room.Type type;

    public Tree(Container container)
    {
        leaf = container;
        type = Room.Type.NONE;
    }

    public List<Tree> getLeafs(List<Tree> trees)
    {
        if (this.leftChild == null || this.rightChild == null)
        {
            trees.Add(this);
            return trees;
        }
        else
        {
            leftChild.getLeafs(trees);
            rightChild.getLeafs(trees);
            return trees;
        }
    }

    public void drawPaths(Tilemap tilemap, TileBase[] tiles)
    {
        if (leftChild != null && rightChild != null)
        {
            leftChild.leaf.createPath(rightChild.leaf, tilemap, tiles);
            leftChild.drawPaths(tilemap, tiles);
            rightChild.drawPaths(tilemap, tiles);
        }
    }

    /// <summary>
    /// Recursively draw the tree
    /// </summary>
    /// <param name="tilemap"></param>
    /// <param name="tileBases"></param>
    public void drawTree(Tilemap tilemap, TileBase[] tileBases)
    {
        this.leaf.drawContainer(tilemap, tileBases);
        if (this.leftChild != null)
        {
            this.leftChild.drawTree(tilemap, tileBases);
        }
        if (this.rightChild != null)
        {
            this.rightChild.drawTree(tilemap, tileBases);
        }
    }

    /// <summary>
    /// Recursively set types for the tree
    /// </summary>
    /// <param name="rule"></param>
    public void setTypes(List<Room.Type> roomsToAdd)
    {

        if (this.leftChild != null)
        {
            leftChild.setTypes(roomsToAdd); 
        }
        if (this.rightChild != null)
        {
            rightChild.setTypes(roomsToAdd);
        }

        if (this.leftChild == null && this.rightChild == null) // leaf
        {
            //Debug.Log(roomsToAdd.Count);
            if (roomsToAdd.Count > 0)
            {
                if (type.Equals(Room.Type.NONE))
                {
                    type = roomsToAdd.ToArray()[0];
                    roomsToAdd.RemoveAt(0);
                }
            }
        }
    }
}
