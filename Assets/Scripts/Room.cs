﻿using UnityEngine;
using System.Collections;
using UnityEngine.Tilemaps;

public class Room
{

    public enum Type
    {
        NONE,

        DUNGEON,
        OBSTACLE,
        LOCK,
        ROOM,
        KEY,
        TREASURE,
        GRASS,

        COUNT
    }

    public int x;
    public int y;
    public int width;
    public int height;

    public Type type;

    public Vector3Int[] doors = new Vector3Int[4];

    public Vector2Int center;

    public Room(Container container, Room.Type type)
    {
        this.x = container.x + Random.Range(1, container.width / 3);
        this.y = container.y + Random.Range(1, container.height / 3);
        this.width = container.width - (Random.Range(1, container.width / 3) * 2);
        this.height = container.height - (Random.Range(1, container.height / 3) * 2);

        center = new Vector2Int(x + (width / 2), y + (height / 2));

        this.type = type;
    }

    public bool isDoorPos(Tilemap tilemap, TileBase[] tiles, int i, int j, bool horizontal)
    {
        Vector3Int left = new Vector3Int(i - 1, j, 0);
        Vector3Int right = new Vector3Int(i + 1, j, 0);
        Vector3Int up = new Vector3Int(i, j + 1, 0);
        Vector3Int down = new Vector3Int(i, j - 1, 0);
        Vector3Int diag1 = new Vector3Int(i + 1, j - 1, 0);
        Vector3Int diag2 = new Vector3Int(i - 1, j - 1, 0);
        if (horizontal)
        {
            return tilemap.GetTile(left) != null
                            && tilemap.GetTile(left).Equals(tiles[0]) // wall
                            && tilemap.GetTile(right) != null
                            && (tilemap.GetTile(right).Equals(tiles[0]))

                            && tilemap.GetTile(up) != null
                            && (tilemap.GetTile(up).Equals(tiles[1]))
                            && tilemap.GetTile(down) != null
                            && (tilemap.GetTile(down).Equals(tiles[1]))
                            ;
        }
        else
        {
            return tilemap.GetTile(left) != null
                && tilemap.GetTile(left).Equals(tiles[1]) // wall
                && tilemap.GetTile(right) != null
                && (tilemap.GetTile(right).Equals(tiles[1]))

                && tilemap.GetTile(up) != null
                && (tilemap.GetTile(up).Equals(tiles[0]))
                && tilemap.GetTile(down) != null
                && (tilemap.GetTile(down).Equals(tiles[0]))
                ;
        }
    }

    // Places the doors correctly
    public void generateDoors(Tilemap tilemap, TileBase[] tiles)
    {
        for (int i = x; i < x + width; i++) // Top Wall Doors
        {
            Vector3Int pos = new Vector3Int(i, y + height, 0);
            if (tilemap.GetTile(pos) != null && tilemap.GetTile(pos).Equals(tiles[1]))
            {
                // GGG 
                // WDW
                // GGG
                if (isDoorPos(tilemap, tiles, i, y + height, true))
                {
                    switch (type)
                    {
                        case Type.LOCK:
                            tilemap.SetTile(new Vector3Int(i, y + height, 0), tiles[6]);
                            break;
                        default:
                            tilemap.SetTile(new Vector3Int(i, y + height, 0), tiles[3]);
                            break;
                    }
                }
            }
        }

        for (int i = x; i < x + width; i++) // Bottom Wall Doors
        {
            Vector3Int pos = new Vector3Int(i, y, 0);
            if (tilemap.GetTile(pos) != null && tilemap.GetTile(pos).Equals(tiles[1]))
            {
                // GGG 
                // WDW
                // GGG
                if (isDoorPos(tilemap, tiles, i, y, true))
                {
                    switch (type)
                    {
                        case Type.LOCK:
                            tilemap.SetTile(new Vector3Int(i, y, 0), tiles[6]);
                            break;
                        default:
                            tilemap.SetTile(new Vector3Int(i, y, 0), tiles[3]);
                            break;
                    }
                }
            }
        }

        for (int i = y; i < y + height; i++) // Left Wall Doors
        {
            Vector3Int pos = new Vector3Int(x, i, 0);
            if (tilemap.GetTile(pos) != null && tilemap.GetTile(pos).Equals(tiles[1]))
            {
                // GWG 
                // GDG
                // GWG
                if (isDoorPos(tilemap, tiles, x, i, false))
                {
                    switch (type)
                    {
                        case Type.LOCK:
                            tilemap.SetTile(new Vector3Int(x, i, 0), tiles[6]);
                            break;
                        default:
                            tilemap.SetTile(new Vector3Int(x, i, 0), tiles[3]);
                            break;
                    }
                }
            }
        }

        for (int i = y; i < y + height; i++) // Right Wall Doors
        {
            Vector3Int pos = new Vector3Int(x + width, i, 0);
            if (tilemap.GetTile(pos) != null && tilemap.GetTile(pos).Equals(tiles[1]))
            {
                // GWG 
                // GDG
                // GWG
                if (isDoorPos(tilemap, tiles, x + width, i, false))
                {
                    switch (type)
                    {
                        case Type.LOCK:
                            tilemap.SetTile(new Vector3Int(x + width, i, 0), tiles[6]);
                            break;
                        default:
                            tilemap.SetTile(new Vector3Int(x + width, i, 0), tiles[3]);
                            break;
                    }
                }
            }
        }

    }

    public void generateWalls(Tilemap tilemap, TileBase[] tiles)
    {
        for (int i = x; i < x + width; i++) // Top Wall 
        {
            Vector3Int pos = new Vector3Int(i, y + height, 0);
            if (tilemap.GetTile(pos) == tiles[1]) continue;
            tilemap.SetTile(pos, tiles[0]);
        }

        for (int i = x; i < x + width; i++) // Bottom Wall
        {
            Vector3Int pos = new Vector3Int(i, y, 0);
            if (tilemap.GetTile(pos) == tiles[1]) continue;
            tilemap.SetTile(pos, tiles[0]);
        }

        for (int i = y; i < y + height; i++) // Left wall
        {
            Vector3Int pos = new Vector3Int(x, i, 0);
            if (tilemap.GetTile(pos) == tiles[1]) continue;
            tilemap.SetTile(pos, tiles[0]);
        }

        for (int i = y; i <= y + height; i++) // Right wall
        {
            Vector3Int pos = new Vector3Int(x + width, i, 0);
            if (tilemap.GetTile(pos) == tiles[1]) continue;
            tilemap.SetTile(pos, tiles[0]);
        }
    }

    // Creates a room with bricks, ground and doors
    // Tiles[0] = bricks/wall
    // Tiles[1] = ground
    public void createRoom(Tilemap tilemap, TileBase[] tiles)
    {
        generateWalls(tilemap, tiles);

        for (int i = x + 1; i < x + width; i++) // Ground
        {
            for (int j = y + 1; j < y + height; j++)
            {
                Vector3Int pos = new Vector3Int(i, j, 0);
                if (tilemap.GetTile(pos) != tiles[0])
                    switch (type)
                    {
                        case Room.Type.GRASS:
                            tilemap.SetTile(pos, tiles[4]);
                            break;
                        default:
                            tilemap.SetTile(pos, tiles[1]);
                            break;
                    }
            }
        }

        switch (type)
        {
            case Room.Type.TREASURE:
                tilemap.SetTile(new Vector3Int(center.x, center.y, 0), tiles[2]);
                break;
            case Room.Type.KEY:
                tilemap.SetTile(new Vector3Int(center.x, center.y, 0), tiles[5]);
                break;
        }
    }

    public override string ToString()
    {
        return "[R:" + type.ToString() + "]";
    }
}
