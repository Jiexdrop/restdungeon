﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Rule 
{
    // dungeon -> obstacle + key + treasure;
    // obstacle -> key + obstacle + lock + obstacle;
    // obstacle -> monster + obstacle;
    // obstacle -> room

    public Room.Type roomType;
    public List<Room.Type> typesToAdd = new List<Room.Type>();

    public Rule(Room.Type roomType)
    {
        this.roomType = roomType;
    }

    public void AddType(Room.Type typeToAdd)
    {
        typesToAdd.Add(typeToAdd);
    }

    public static List<Rule> GetRules()
    {
        // Define grammar rules
        Rule dungeonGrammar = new Rule(Room.Type.DUNGEON);
        dungeonGrammar.AddType(Room.Type.OBSTACLE);
        dungeonGrammar.AddType(Room.Type.KEY);
        dungeonGrammar.AddType(Room.Type.TREASURE);

        Rule obstacle1 = new Rule(Room.Type.OBSTACLE);
        obstacle1.AddType(Room.Type.KEY);
        obstacle1.AddType(Room.Type.OBSTACLE);
        obstacle1.AddType(Room.Type.LOCK);
        obstacle1.AddType(Room.Type.OBSTACLE);

        Rule obstacle2 = new Rule(Room.Type.OBSTACLE);
        obstacle2.AddType(Room.Type.GRASS);
        obstacle2.AddType(Room.Type.OBSTACLE);

        Rule obstacle3 = new Rule(Room.Type.OBSTACLE);
        obstacle3.AddType(Room.Type.ROOM);

        List<Rule> grammars = new List<Rule>();
        grammars.Add(dungeonGrammar);
        grammars.Add(obstacle1);
        grammars.Add(obstacle2);
        grammars.Add(obstacle3);

        return grammars;
    }
}

class RuleTree
{
    public Room.Type type;

    public GameObject myself;

    public List<RuleTree> children = new List<RuleTree>();

    public RuleTree(Room.Type type, GameObject parent)
    {
        this.type = type;
        myself = new GameObject(type.ToString());
        if (parent != null)
        {
            myself.transform.parent = parent.transform;
        }
    }

    public bool NotARoom()
    {
        return type.Equals(Room.Type.OBSTACLE) || type.Equals(Room.Type.DUNGEON);
    }

    public bool IsARoom()
    {
        return !NotARoom();
    }

    public void Add(RuleTree tree)
    {
        children.Add(tree);
    }
}