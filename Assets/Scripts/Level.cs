﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using Random = UnityEngine.Random;

public class Level : MonoBehaviour
{
    [SerializeField]
    private Tilemap tilemap = null;

    [SerializeField]
    private TileBase[] mainTiles = null;

    [SerializeField]
    Grid grid;

    private Dictionary<Vector3Int, TileBase> editMap = new Dictionary<Vector3Int, TileBase>();

    bool gameOfLife = false;

    public int SUBDIVISIONS = 4;

    private Rule dungeon = new Rule(Room.Type.DUNGEON);

    void Start()
    {
        Create();
    }


    private void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            if (gameOfLife)
            {
                gameOfLifeUpdate();
            }
            else
            {
                tilemap.ClearAllTiles();
                Create();
            }
        }
    }

    void Create()
    {
        Container container = new Container(-32, -32, 64, 64);
        //Container container = new Container(0, 0, 64,64);
        Tree dungeonTree = container.splitContainer(container, SUBDIVISIONS);

        //dungeonTree.drawTree(tilemap, mainTiles);
        List<Tree> dungeonTrees = new List<Tree>();
        dungeonTree.getLeafs(dungeonTrees);


        List<Room.Type> roomsToAdd = new List<Room.Type>();

        int count = 0;

        while (count < SUBDIVISIONS || count > (SUBDIVISIONS * SUBDIVISIONS))
        {
            roomsToAdd.Clear();
            // Build grammar tree of rooms to add
            RuleTree t = buildRandomRules(new RuleTree(Room.Type.DUNGEON, null), Rule.GetRules());
            count = GetRoomsToAdd(t, roomsToAdd);
            if (count < SUBDIVISIONS || count > (SUBDIVISIONS * SUBDIVISIONS)) {
                Destroy(t.myself);
            } else {
                Debug.Log("GEN WITH " + count + " ROOMS");
            }
        }

        //foreach(Room.Type r in roomsToAdd)
        //{
        //    Debug.Log(r.ToString());
        //}

        // Set types of room recursively for each leaf
        dungeonTree.setTypes(roomsToAdd);

        List<Room> rooms = new List<Room>();
        foreach (Tree tree in dungeonTrees)
        {
            Room room = new Room(tree.leaf, tree.type);
            rooms.Add(room);
        }

        foreach (Room room in rooms)
        {
            room.createRoom(tilemap, mainTiles);
        }

        dungeonTree.drawPaths(tilemap, mainTiles);


        foreach (Room room in rooms)
        {
            room.generateDoors(tilemap, mainTiles);
        }

        //container.drawContainer(tilemap, mainTiles);


        // Game of life
        if (gameOfLife)
        {
            for (int i = -10; i < 10; i++)
            {
                for (int j = -10; j < 10; j++)
                {
                    Vector3Int pos = new Vector3Int(i, j, 0);
                    if (!tilemap.HasTile(pos))
                    {
                        tilemap.SetTile(new Vector3Int(i, j, 0), mainTiles[1]);
                    }
                }
            }
        }
    }


    private RuleTree buildRandomRules(RuleTree ruleTree, List<Rule> rules, int maxDepth = 6)
    {

        List<Room.Type> randomTypes = GetRandomTypesToAdd(ruleTree.type, rules);


        if (randomTypes != null)
        {
            foreach (Room.Type t in randomTypes)
            {
                ruleTree.Add(new RuleTree(t, ruleTree.myself));
            }

            foreach (RuleTree gt in ruleTree.children)
            {
                if (maxDepth > 0)
                {
                    buildRandomRules(gt, rules, maxDepth--);
                }
            }
        }


        return ruleTree;
    }


    private List<Room.Type> GetRandomTypesToAdd(Room.Type type, List<Rule> rules)
    {
        List<Rule> typeRules = new List<Rule>();

        foreach (Rule g in rules)
        {
            if (g.roomType.Equals(type))
            {
                typeRules.Add(g);
            }
        }

        if (typeRules.Count > 0)
        {
            return typeRules[Random.Range(0, typeRules.Count)].typesToAdd;
        }


        return null;
    }

    private int GetRoomsToAdd(RuleTree ruleTree, List<Room.Type> toAdd)
    {
        int count = 0;

        if (ruleTree.IsARoom())
        {
            toAdd.Add(ruleTree.type);
            count++;
        }

        foreach (RuleTree t in ruleTree.children)
        {
            count += GetRoomsToAdd(t, toAdd);
        }

        return count;
    }

    void gameOfLifeUpdate()
    {
        editMap.Clear();
        for (int i = -10; i < 10; i++)
        {
            for (int j = -10; j < 10; j++)
            {
                Vector3Int pos = new Vector3Int(i, j, 0);

                //Debug.Log(neighboorCells(pos));

                if (tilemap.GetTile(pos).GetInstanceID() == mainTiles[0].GetInstanceID())
                {
                    if (neighboorCells(pos) < 2)
                    {
                        editMap.Add(pos, mainTiles[1]); // die
                    }
                    else if (neighboorCells(pos) > 3)
                    {
                        editMap.Add(pos, mainTiles[1]); // die potatoes
                    }
                    else if ((neighboorCells(pos) == 2 || neighboorCells(pos) == 3))
                    {
                        editMap.Add(pos, mainTiles[0]); //reproduce
                    }
                }
                else if (tilemap.GetTile(pos).GetInstanceID() == mainTiles[1].GetInstanceID())
                {
                    if (neighboorCells(pos) == 3)
                    {
                        editMap.Add(pos, mainTiles[0]);
                    }
                }
            }
        }

        foreach (KeyValuePair<Vector3Int, TileBase> entry in editMap)
        {
            tilemap.SetTile(entry.Key, entry.Value);
        }
    }


    int neighboorCells(Vector3Int pos)
    {
        int count = 0;
        Vector3Int cpy = pos;
        cpy.x += 1;

        if (tilemap.GetTile(cpy) != null && tilemap.GetTile(cpy).GetInstanceID() == mainTiles[0].GetInstanceID()) // alive tile
        {
            count++;
        }

        cpy = pos;
        cpy.x -= 1;
        if (tilemap.GetTile(cpy) != null && tilemap.GetTile(cpy).GetInstanceID() == mainTiles[0].GetInstanceID()) // alive tile
        {
            count++;
        }

        cpy = pos;
        cpy.y += 1;
        if (tilemap.GetTile(cpy) != null && tilemap.GetTile(cpy).GetInstanceID() == mainTiles[0].GetInstanceID()) // alive tile
        {
            count++;
        }

        cpy = pos;
        cpy.y -= 1;
        if (tilemap.GetTile(cpy) != null && tilemap.GetTile(cpy).GetInstanceID() == mainTiles[0].GetInstanceID()) // alive tile
        {
            count++;
        }

        cpy = pos;
        cpy.y += 1;
        cpy.x += 1;
        if (tilemap.GetTile(cpy) != null && tilemap.GetTile(cpy).GetInstanceID() == mainTiles[0].GetInstanceID()) // alive tile
        {
            count++;
        }

        cpy = pos;
        cpy.y -= 1;
        cpy.x -= 1;
        if (tilemap.GetTile(cpy) != null && tilemap.GetTile(cpy).GetInstanceID() == mainTiles[0].GetInstanceID()) // alive tile
        {
            count++;
        }

        cpy = pos;
        cpy.y -= 1;
        cpy.x += 1;
        if (tilemap.GetTile(cpy) != null && tilemap.GetTile(cpy).GetInstanceID() == mainTiles[0].GetInstanceID()) // alive tile
        {
            count++;
        }

        cpy = pos;
        cpy.y += 1;
        cpy.x -= 1;
        if (tilemap.GetTile(cpy) != null && tilemap.GetTile(cpy).GetInstanceID() == mainTiles[0].GetInstanceID()) // alive tile
        {
            count++;
        }

        return count;
    }

    void CreatePath(int x1, int y1, int x2, int y2, TileBase wall)
    {
        int y; float dx; float dy;

        y = y1;

        dx = x2 - x1;
        dy = y2 - y1;

        // erreur
        float e = 0.0f;
        float e1 = dx / dy;
        float e2 = -1.0f;

        for (int x = x1; x < x2; x++)
        {
            tilemap.SetTile(new Vector3Int(x, y, 0), wall);
            if ((e = e + e1) >= 0.5f)
            {
                y++;
                e = e + e2;
            }
        }
    }


}
